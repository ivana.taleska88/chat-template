$(function () {

    // gets the value of the input
    $(".button").click(function () {
        var chat = $('.message').val();
        // console.log(chat);
        $('.me').append(chat);
    });

    //chat-active onclick
    $('.button').on('click', function () {
        myMessage();
    });

    //chat-active enter
    $('.input-group-prepend').keypress(function (e) {
        var key = e.which;
        if (key == 13) {
            myMessage();
        }
    });

    // funkcija za vnes vo input
    function myMessage() {

        //vnes preku input
        var message = $('.message').val();
        $('.message').addClass('.clicked');

        //data pod input vnes
        new Date($.now());
        var dt = new Date();
        var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        //apend na val od input i odgovor
        if (message != '' && ($(".message").hasClass("clicked") === false))// ne praka prazen '';
        {
            $('.chat-val').append('<p class="align-self-end firstApend">' + message +
                '</p>' + '<p style="font-size: 8px;" class="text-right">'
                + time + '</p>' + '<p class="text-left secondApend"> Hello Ivana </p>' + '<p style="font-size: 8px;" class="text-left">' + time + '</p>'
            );
        };
        // chisti value od input
        $('.message').val('');
        // vraka za edna sekunda
        setTimeout(function () {
            $('.secondApend').show();
        }, 1000);
    };



    // konstruktor person
    function person(name, surname, img) {
        this.name = name;
        this.surname = surname;
        this.img = img;
        this.print = function () {
            return `<li class="text-left hover clickShow">
        <img class="img-responsive img-circle pimg float-left" src="${this.img}">
        <span class="text-left">${this.name} ${this.surname}</span>
        <i class="fas fa-circle float-right" style="padding-top: 10px"></i></li>`;
        };
    };

    var john = new person("John", "Down", "person.png");
    var smith = new person("Smith", "Lawrence", "person.png");
    var josh = new person("Josh", "Dino", "person.png");
    var petar = new person("Petar", "Pan", "person.png");
    var maya = new person("Maya", "Matinanin", "person.png");
    var sarah = new person("Sarah", "Fagan", "person.png");
    var kate = new person("Kate", "Middleton", "person.png");
    var michael = new person("Michael", "Lipski", "person.png");
    var nick = new person("Nick", "Sloter", "person.png");

    var persons = [john, smith, josh, petar, maya, sarah, kate, michael, nick];
    // console.log(persons);


    //gi printa na html
    for (var i = 0; i < persons.length; i++) {
        $("#list").append(persons[i].print());
    }

    //shows and hides the chat
    $('.clickShow').on('click', function () {
        $('#hide').toggle();
    });

    // click na X za da se zatvora chatot          
    function xClose() {
        $('.fa-times').on('click', function () {
            $('.card').animate({ height: 50, marginTop: 280 }, "slow");
            $('.input-group-prepend').hide("slow");
            $(".fa-times").hide();
            $(".fa-square").show();
        });
    };
    xClose();

    function xOpen() {
        $('.fa-square').on('click', function () {
            $('.card').animate({ height: 350, marginTop: 0 }, "slow");
            $('.input-group-prepend').show("slow");
            $(".fa-times").show();
            $(".fa-square").hide();
        });
    };
    xOpen()


    // person onclick
    $(document).on("click", "li", function () {
        $("#hide").slideDown('fast');
        $('.personInfo').html(`<img class="img-responsive img-circle pimg" style="margin-right: 10px;" src="${$(this).find("img").attr("src")}"><span>${$(this).find("span").html()}</span><i class="fas fa-times float-right"
    style="margin-left: 15px" ></i><i class="far fa-square float-right" style="margin-left: 15px; display: none;" ></i>`);
        $('.chat-val').html('');
        $('.fa-times').on("click", xClose());
        $('.fa-square').on("click", xOpen());   

    });



});
